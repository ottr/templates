if [ ! -f lutra.jar ]; then
    wget https://ottr.xyz/downloads/lutra/lutra-master.jar -O lutra.jar
fi

java -jar lutra.jar --mode formatLibrary -l . -L stottr -e "stottr" -O stottr -o .build
java -jar lutra.jar --mode formatLibrary -l . -L stottr -e "stottr" -O wottr -o .build
java -jar lutra.jar --mode docttrLibrary -l .build -L stottr -e "stottr" -o .build

cp -R .build/tpl.ottr.xyz/* .deploy

find .deploy/ -type d -exec sh -c '\
     (cd $1 && \
     	 find * | grep -e ".*\.ttl" > contents-index.txt && \
     	 zip -r contents.zip . -i *.ttl -i contents-index.txt)' _ {} \;



